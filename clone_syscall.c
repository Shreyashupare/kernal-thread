#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "mmu.h"
#define CLONE_THREAD 1
#define CLONE_PARENT 2
#define CLONE_VM 4
int clone_thread = 1;
int clone_parent = 0;
int clone_vm = 0;
void fun1(void *a);
void fun2(void *a);
void kill_thread(void *a);
void thread_call(void *a);
void clonethreadflag(void *gpid);
void matrix_mul(void *parts);
void multiple_threads(void);
void (*fptr1)(void *) = &fun1;
void (*fptr2)(void *) = &fun2;

int row1 = 3, col1 = 3, row2 = 3, col2 = 3;
int Matrix1[3][3] = {{1, 1, 1}, {2, 2, 2}, {3, 3, 3}};
int Matrix2[3][3] = {{3, 3, 3}, {2, 2, 2}, {1, 1, 1}};
int Matrix3[3][3];

int main()
{
    uint args[2];
    args[0] = 3;
    args[1] = 4;
    int flags = 0;
    if (clone_thread == 1)
    {
        flags = flags + CLONE_THREAD;
    }
    if (clone_parent == 1)
    {
        flags = flags + CLONE_PARENT;
    }
    if (clone_vm == 1)
    {
        flags = flags + CLONE_VM;
    }
    //thread 1
    printf(1, "calling thread1 by process...\n");
    int tid = thread_create((void *)fptr1, (void *)args, flags);
    thread_join(tid);
    //thread 2
    uint args2[2] = {100, 2};
    printf(1, "calling thread2 by process...\n");
    int tid2 = thread_create((void *)fptr2, (void *)args2, flags);
    thread_join(tid2);
    //thread to kill
    int argt[2];
    argt[0] = 4;
    argt[1] = 4;
    printf(1, "calling thread to kill...\n");
    int tid3 = thread_create(&kill_thread, (void *)argt, flags);
    thread_join(tid3);

    //thread call to thread which call another thread
    uint args4[1] = {50};
    printf(1, "calling thread which call another thread...\n");
    int tid4 = thread_create(&thread_call, (void *)args4, flags);
    thread_join(tid4);

    // //set flag thread_clone
    // int tgrpid = getppid();
    // printf(1, "onenumberid%d\n", tgrpid);
    // uint args6[1] = {tgrpid};
    // int tid6 = thread_create(&clonethreadflag, (void *)args6, CLONE_THREAD);
    // thread_join(tid6);

    //matrix multiplication using thread
    int r = row1 / 3;
    uint parts1[2] = {0, r};
    int mthread1 = thread_create(&matrix_mul, (void *)parts1, CLONE_VM);
    thread_join(mthread1);
    uint parts2[2] = {r, 2 * r};
    int mthread2 = thread_create(&matrix_mul, (void *)parts2, CLONE_VM);
    thread_join(mthread2);
    uint parts3[2] = {2 * r, row1};
    int mthread3 = thread_create(&matrix_mul, (void *)parts3, CLONE_VM);
    thread_join(mthread3);

    //print matrix
    printf(1, "Matrix after multiplication\n");
    for (int i = 0; i < row1; i++)
    {
        for (int j = 0; j < col2; j++)
        {
            printf(1, "%d ", Matrix3[i][j]);
        }
        printf(1, "\n");
    }
    //run multiple threads in loop
    printf(1, "Running multiple threads in loop\n");
    multiple_threads();
    printf(1, "n");
    exit();
}
void fun1(void *a)
{
    int *arg = (int *)a;
    int value = arg[0];
    int value2 = arg[1];
    int v = value + value2;
    printf(1, "%d\n", v);
    printf(1, "\n");
    exit();
}
void fun2(void *a)
{

    int *arg = (int *)a;
    int value = arg[0];
    int value2 = arg[1];
    int v = value * value2;
    printf(1, "%d\n", v);
    printf(1, "\n");
    exit();
}
//function to kill thread
void kill_thread(void *a)
{
    int *arg = (int *)a;
    int value1 = arg[0];
    int value2 = arg[1];
    int v = value1 + value2;
    int thid = gettid();
    printf(1, "killing thread- id = %d\n", thid);
    printf(1, "\n");
    tkill(thid);
    printf(1, "Thread killed before printing this%d\n", v);
    exit();
}
//thread called using thread
void thread_call(void *a)
{
    int *arg = (int *)a;
    int value = arg[0];
    uint args5[2];
    args5[0] = 1;
    args5[1] = 1;
    printf(1, "calling thread inside thread...\n");
    int tid5 = thread_create((void *)fptr1, (void *)args5, 1);
    thread_join(tid5);
    printf(1, "thread called by thread  exit\n");
    printf(1, "%d\n", value);
    printf(1, "\n");
    exit();
}
// check the group id of parent and thread
void clonethreadflag(void *gpid)
{
    int *grpid = (int *)gpid;
    int tgid = grpid[0];
    int new_tgid = gettgid();
    printf(1, "twoshamenid%d\n", tgid);
    printf(1, "twonumberid%d\n", new_tgid);
    if (tgid == new_tgid)
    {
        printf(1, "thread and process are in same threadgroup\n");
        printf(1, "\n");
    }
    else
    {
        printf(1, "thread and process are not in same threadgroup\n");
        printf(1, "\n");
    }
    exit();
}
//Matrix multiplication
void matrix_mul(void *parts)
{
    int *part = (int *)parts;
    int start = part[0];
    int end = part[1];
    int sum = 0;
    for (int i = start; i < end; i++)
    {
        for (int j = 0; j < col2; j++)
        {
            for (int k = 0; k < row2; k++)
            {
                sum = sum + Matrix1[i][k] * Matrix2[k][j];
            }
            Matrix3[i][j] = sum;
            sum = 0;
        }
    }

    exit();
}
//run multiple threads
void increment(void *args)
{
    int *x = (int *)args;
    for (int i = 0; i < 100; i++)
    {
        *x += 1;
    }
    exit();
}

void multiple_threads(void)
{
    int threads[50];
    int value = 0;
    for (int i = 0; i < 50; i++)
    {
        threads[i] = thread_create(increment, (void *)&value, CLONE_VM);
        thread_join(threads[i]);
    }
    if (value == 5000)
    {
        printf(1, "Successful\n");
    }
    else
    {
        printf(1, "Failed\n");
    }
}
